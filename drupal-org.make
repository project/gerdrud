api = 2
core = 7.26

projects[ckeditor] = 1.13
projects[ctools] = 1.3
projects[entity] = 1.3
projects[jquery_update] = 2.3
projects[libraries] = 2.1
projects[module_filter] = 1.8
projects[panels] = 3.3
projects[pathauto] = 1.2
projects[pathologic] = 2.12
projects[piwik] = 2.4
projects[rules] = 2.6
projects[token] = 1.5
projects[transliteration] = 3.1
projects[views] = 3.7
projects[views_bulk_operations] = 3.2
projects[webform] = 3.19

projects[bootstrap] = 3.0

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.3.2/ckeditor_4.3.2_full.zip"
libraries[ckeditor][directory_name] = "ckeditor"